//
//  TouchDotView.swift
//  NumberRecognition
//
//  Created by Alexander Eichhorn on 15.01.18.
//  Copyright © 2018 Alexander Eichhorn. All rights reserved.
//

import UIKit

//@IBDesignable
class TouchDotView: UIView {

    /*override init(frame: CGRect) {
        super.init(frame: frame)
        draw(frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    */
    override func layoutIfNeeded() {
        shapeLayer?.removeFromSuperlayer()
        draw(self.frame)
    }
    
    var shapeLayer: CAShapeLayer?
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        let path = UIBezierPath(ovalIn: rect)
        
        shapeLayer = CAShapeLayer()
        shapeLayer?.path = path.cgPath
        shapeLayer?.fillColor = UIColor.gray.cgColor
        
        layer.addSublayer(shapeLayer!)
    }
    

}

//
//  Helper.swift
//  NumberRecognition
//
//  Created by Alexander Eichhorn on 15.01.18.
//  Copyright © 2018 Alexander Eichhorn. All rights reserved.
//

import Foundation
import CoreMotion

/// Saves a single sample of 3-axis accelerometer and attitude (pitch, roll, yaw) data
struct MotionData {
    var ax: Double
    var ay: Double
    var az: Double
    let pitch: Double
    let roll: Double
    let yaw: Double
    
    
    /// - returns: Struct converted in dictionary
    func toDict() -> [String: Double] {
        var dict = [String: Double]()
        dict["ax"] = ax
        dict["ay"] = ay
        dict["az"] = az
        dict["pitch"] = pitch
        dict["roll"] = roll
        dict["yaw"] = yaw
        
        return dict
    }
    
    //private var corrected = false
    
    /// rotates coordinate system of internal accelerometer vector (ax, ay, az) by the amount the device was rotated on recording (pitch, roll, yaw)
    mutating func correctForOrientation() {
      //  if corrected { return } // 2 times correction would destroy data
        let matrix = CMRotationMatrix(m11: cos(yaw)*cos(roll), m12: cos(yaw)*sin(roll)*sin(pitch)-sin(yaw)*cos(pitch), m13: cos(yaw)*sin(roll)*cos(pitch)+sin(pitch)*sin(yaw), /* line 1 */
            m21: sin(yaw)*cos(roll), m22: sin(pitch)*sin(roll)*sin(yaw)+cos(pitch)*cos(yaw), m23: sin(yaw)*sin(roll)*cos(pitch)-cos(yaw)*sin(pitch), /* line 2 */
            m31: -sin(roll), m32: cos(roll)*sin(pitch), m33: cos(roll)*cos(pitch))
        
        let (axx, ayy, azz) = (ax, ay, az)
        ax = matrix.m11 * axx + matrix.m12 * ayy + matrix.m13 * azz
        ay = matrix.m21 * axx + matrix.m22 * ayy + matrix.m23 * azz
        az = matrix.m31 * axx + matrix.m32 * ayy + matrix.m33 * azz
        
        //corrected = true
    }
}


func get1DMotionDataArray(recording: [MotionData]) -> [Double] {
    var output = [Double]()
    for r in recording {
        output += [r.ax, r.ay, r.az]
    }
    return output
}



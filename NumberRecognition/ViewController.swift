//
//  ViewController.swift
//  NumberRecognition
//
//  Created by Alexander Eichhorn on 18.12.17.
//  Copyright © 2017 Alexander Eichhorn. All rights reserved.
//

import UIKit
import CoreMotion
import CoreML

class ViewController: UIViewController {

    let model = NumberRecognition() // ML model
    let motionManager = CMMotionManager()
    var motionData = [MotionData]()
    
    var isRecording = false
    
    var touchView: UIView?
    @IBOutlet var trainingSwitch: UISwitch!
    @IBOutlet var trainingNumberSegment: UISegmentedControl!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var probabilityLabel: UILabel!
    
    let MAX_SECS = 5.0 // maximum record time
    var MAX_SAMPLES: Int {
        return Int(MAX_SECS * 10)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // check if current device has a accelerometer sensor built-in
        if !motionManager.isDeviceMotionAvailable {
            let alertController = UIAlertController(title: "Device motion recognition isn't available on this device", message: "", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            // wait until views are loaded
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                self.present(alertController, animated: true, completion: nil)
            })
        }
        
        // no training as default & hide all result labels
        trainingSwitch.isOn = false
        trainingNumberSegment.isHidden = true
        numberLabel.text = ""
        probabilityLabel.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Start to record accelerometer data asynchronously to motionData array until stopped by stopDataRecording()
    */
    func startDataRecording() {
        isRecording = true
        motionData = []
        motionManager.deviceMotionUpdateInterval = 0.1 //10 Hz
        motionManager.startDeviceMotionUpdates(using: .xArbitraryZVertical, to: .main) { (motion, error) in
            
            if let error = error {
                print("error on device motion update: \(error.localizedDescription)")
                return
            }
            guard let motion = motion else { return }
            print("ax: \(motion.userAcceleration.x), ay: \(motion.userAcceleration.y), az: \(motion.userAcceleration.z)")
            //print("pitch: \(motion.attitude.pitch*180 / .pi), roll: \(motion.attitude.roll*180 / .pi), yaw: \(motion.attitude.yaw*180 / .pi)")
            
            self.motionData.append(MotionData(ax: motion.userAcceleration.x, ay: motion.userAcceleration.y, az: motion.userAcceleration.z, pitch: motion.attitude.pitch, roll: motion.attitude.roll, yaw: motion.attitude.yaw))
            
            /*if self.motionData.count >= self.MAX_SAMPLES {
                self.stopDataRecording() // stop recording automatically when time is up
            }*/
        }
    }
    
    /**
     Stops and processes recording of accelerometer data
    */
    func stopDataRecording() {
        motionManager.stopDeviceMotionUpdates()
        isRecording = false
        handleDataRecording() // process recorded data
    }
    
    /**
     Processes recorded accelerometer data in motionData array differently based on current UI switch state.
     * **If on:** Uploads data to server (for training)
     * **If off**: Sends data to internal ML model
    */
    func handleDataRecording() {
        if motionData.count < 10 { print("less than 1 second of data"); return }
        if motionData.count > MAX_SAMPLES { motionData.removeSubrange(MAX_SAMPLES...) } // remove more than 50 samples
        print("recorded \(motionData.count) samples")
        
        if trainingSwitch.isOn {
            trainDataRecording()
            return
        }
        recognizeNumber()
        
    }
    
    /**
     Uploads current accelerometer recording and wanted result (segment picker) to web server (for training)
    */
    func trainDataRecording() {
        let wantedResult = trainingNumberSegment.selectedSegmentIndex
        let dictData = motionData.map { (item) -> [String: Double] in
            return item.toDict()
        }
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dictData, options: .init(rawValue: 0))
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print("json: \(jsonString)")
        
        let url = URL(string: "https://losjet.com/tests/number_recognizer/train_upload.php")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postString = "wantedResult=\(wantedResult)&motionData=\(jsonString)"
        request.httpBody = postString.data(using: .utf8)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error uploading test motion: \(error.localizedDescription)")
            }
            guard let data = data else { return }
            guard let dataString = String(data: data, encoding: .utf8) else { return }
            
            print("uploaded test motion with response: \(dataString)")
            
        }.resume()
        
        
    }
    
    /**
     recognize number with recorded accelerometer data using CoreML and display result on screen
     */
    func recognizeNumber() {
        
        for i in 0...motionData.count-1 { motionData[i].correctForOrientation() } //correct every datapoint for its orientation
        
        let unshapeArray = get1DMotionDataArray(recording: motionData)
        let inputArray = try! MLMultiArray(shape: [NSNumber(value: MAX_SAMPLES*3)], dataType: MLMultiArrayDataType.double)
        for i in 0...(MAX_SAMPLES*3)-1 { //fill up with zeroes
            inputArray[i] = NSNumber(floatLiteral: 0.0)
        }
        for (i, e) in unshapeArray.enumerated() {
            inputArray[i] = NSNumber(floatLiteral: e)
        }
        
        guard let output = try? model.prediction(input: NumberRecognitionInput(input1: inputArray)) else { print("Error while trying to use CoreML"); return }
        
        
        // find max probability
        var max = (value: 0, probability: 0.0)
        for i in 0...9 {
            let currentProbability = Double(truncating: output.output1[i])
            if currentProbability > max.probability {
                max.value = i
                max.probability = currentProbability
            }
            print("\(i): \(currentProbability)")
        }
        
        numberLabel.text = "\(max.value)"
        probabilityLabel.text = "\(Int(max.probability*100)) %"
    }
    
    
    // MARK: - Touches Delegates
    
    /**
     Called when finger starts touching the screen.
     Starts data recording and animated circle growing bigger over time
    */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touch began")
        startDataRecording()
        
        guard let firstTouch = touches.first else { return }
        let startFrame = CGRect(origin: firstTouch.location(in: self.view), size: CGSize(width: 1, height: 1))
        
        touchView?.removeFromSuperview()
        touchView = UIView(frame: startFrame)
        touchView?.backgroundColor = .white
        touchView?.layer.cornerRadius = 0.5
        self.view.insertSubview(touchView!, at: 0)
        UIView.animate(withDuration: MAX_SECS, delay: 0, options: .curveEaseOut, animations: {
            let desiredSize: CGFloat = 400
            self.touchView?.frame = CGRect(x: startFrame.origin.x - desiredSize/2, y: startFrame.origin.y - desiredSize/2, width: desiredSize, height: desiredSize)
            self.touchView?.layer.cornerRadius = desiredSize/2
            self.touchView?.backgroundColor = #colorLiteral(red: 0.8719033355, green: 0.8719033355, blue: 0.8719033355, alpha: 1)
        }, completion: nil)
        
    }
    
    /**
     Called when finger was removed from screen.
     Stops data recording and removes animated circle from UI.
    */
    func touchesFinished(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touch ended")
        stopDataRecording()
        touchView?.removeFromSuperview()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesFinished(touches, with: event)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesFinished(touches, with: event)
    }
    
    
    /**
     called when switch state in UI changed
    */
    @IBAction func trainingSwitched(_ sender: Any?) {
        if trainingSwitch.isOn {
            trainingNumberSegment.isHidden = false
            numberLabel.text = ""
            probabilityLabel.text = ""
        } else {
            trainingNumberSegment.isHidden = true
        }
    }

}


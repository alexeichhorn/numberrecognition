//
//  NumberRecognitionTests.swift
//  NumberRecognitionTests
//
//  Created by Alexander Eichhorn on 18.12.17.
//  Copyright © 2017 Alexander Eichhorn. All rights reserved.
//

import XCTest
@testable import NumberRecognition

class NumberRecognitionTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRotationMatrix() {
        
        var motionData = MotionData(ax: 0.41, ay: 0.21, az: -0.69, pitch: 1.2, roll: -0.1, yaw: 0.7)
        motionData.correctForOrientation()
        
        XCTAssert(-0.14715 > motionData.ax && motionData.ax > -0.14716)
        XCTAssert(0.816378 > motionData.ay && motionData.ay > 0.816377)
        XCTAssert(-0.01309 > motionData.az && motionData.az > -0.01310)
        
    }
    
}
